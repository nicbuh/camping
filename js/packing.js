// Find Element in DOM
let messageElement = document.getElementById("packingListMessage");

// Element = display none
messageElement.style.display = "none";

function check() {
  // Find all checkboxes in DOM
  let allInputs = document.getElementsByTagName("input");

  // Are all checkboxes checked?
  let allChecked = true;
  for (let i = 0; allInputs.length > i; i++) {
    allChecked = allChecked && allInputs[i].checked;
  }
  //show message if all checkboxes are checked
  messageElement.style.display = allChecked ? "block" : "none";
}
