var monument = [8.22, 46.81];
var map = new maplibregl.Map({
  container: "map",
  style:
    "https://api.maptiler.com/maps/streets/style.json?key=get_your_own_OpIi9ZULNHzrESv6T2vL",
  center: [8.22, 46.81],
  zoom: 6.5,
});

// create the popup
var popup = new maplibregl.Popup({ offset: 25 }).setText(
  "In the middle of Switzerland"
);

// create DOM element for the marker
var el = document.createElement("div");
el.id = "marker";
el.style.backgroundImage = 'url("images/mountain.jpg")';

// create the marker
new maplibregl.Marker(el)
  .setLngLat(monument)
  .setPopup(popup) // sets a popup on this marker
  .addTo(map);

// disable map zoom when using scroll
map.scrollZoom.disable();

map.on("click", function (e) {
  map.scrollZoom.enable();
});

